# tn-test

[Live here](https://imurtazahashmi.gitlab.io/tn-test/)

URLs that return File/Folder

www.example.com/testFile.txt

www.abc.com/testFolder

## Problem

You shall implement a very simple browser application in Javascript or Typescript that shall allow a user to check if an entered URL exists. The user shall be able to enter an URL and the URL must then be checked for valid format and if the format is correct it shall be sent to a server which provides the information if the URL exits and if it is a file or a folder. You shall not implement the server side, but just mock it on the client. The server call shall be asynchronous.
The check for the URL format and the existence check shall be triggered as the user is typing, but the existence check shall be throttled to avoid that too many server requests are done all the time.

## Screenshot

Finding File

![image.png](./public/src/image.png)

Finding Folder

![image2.png](./public/src/image2.png)

Folder not available on different URL

![image3.png](./public/src/image3.png)

## Description

For throttling, instead of looking up on each key stroke, I waited till the user has completed the basic URL, for this example I set the URL to 'www.example.com'. I get this idea from implementing a shortURL for a project, we need the path after let's say 'tinyurl.com/path' to look for the file/folder, or even DB lookup. The idea here is to wait, till the right information is available. Another way of throttling could be limiting number of calls for a duration period, for example in addition to above, we have file names ranging up to 15 characters long, we can limit lookup 20 lookups for 5 seconds etc. Or set it to do lookups for entire URL as user types till the lookup limit is reached. The implementation for that part is simple but not part of this code.

To check valid URL used a simple regex picked up from internet, a more thorough regex could be needed for more accurate results. Also checking some basic file types using regex.

#### Update

Added throttling which adds 1s delay for consecutive server calls, and a mock server that responds with 1s delay.


