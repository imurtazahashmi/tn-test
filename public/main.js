// regex to check for valid URL
const URLregex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
// URL regex to get file name
var sampleURLregex = /www\..*\.com./i // /www\.example\.com./i
// checking a few file types
var fileRegex = /^.*\.(jpg|json|txt|gif|GIF|doc|DOC|pdf|PDF)/
// mock server contents - used in lookup
var serverContents = {
    'testFile.txt':'content',
    'www.abc.com/testFolder':'/',
    'www.example.com/testFile.txt':'/',
}
// mock server and throttle time delays
let serverDelay = 1000
let throttleTime = 1000


let urlField = document.getElementById('url')
var resultMsg = document.getElementById('resultMsg')
var throttleServer = throttleCalls((url)=>{findFileOrFolder(url)},throttleTime)

urlField.addEventListener('input', function(event) {
    let url = event.target.value
    updateField(checkURL(url))
    // //&& sampleURLregex.test(url) -> this condition in if, to check for a relevant URL
    if(checkURL(url) ){ 
        throttleServer(url)
        
    } else {
        resultMsg.innerText =' '
    } 
  });

  var handleFindClick = () => {
    let url=urlField.value
    if(!checkURL(url)){
        resultMsg.innerText ='URL not valid'
        return
    }
    if(!sampleURLregex.test(url)){
        resultMsg.innerText ='File/Folder not found'
        return 
    } 
    findFileOrFolder(url.split(sampleURLregex)[1])
}

///// Throttling server calls
function throttleCalls(callBack, delay = 1000) {

    let shouldWait = false
    let waitingArgs // saving updated arguments, if waiting time is on going
    const timeoutFunc = () => {
      if (waitingArgs == null) {
        shouldWait = false
      } else {
        callBack(...waitingArgs)
        waitingArgs = null
        setTimeout(timeoutFunc, delay)
      }
    }
  
    return (...args) => {
      if (shouldWait) {
        waitingArgs = args
        return
      }
  
      callBack(...args)
      shouldWait = true
  
      setTimeout(timeoutFunc, delay)
    }
  }


///// Mock server call
async function findFileOrFolder(lookup){
    if(lookup=='' || lookup=='/'){
        resultMsg.innerText ='Empty path'
        return
    }
    let serverCall = delay => new Promise((resolve,reject) =>{
        let result=lookup in serverContents
        setTimeout(()=>{
            if(result)
                resolve('done')
            else
                reject('failed')
        }, delay)
    }) 

    await serverCall(serverDelay).then((msg)=>{
        lookup=lookup.split(sampleURLregex)[1]
        console.log(lookup)
        resultMsg.innerText = (fileRegex.test(lookup)?'Found File: ':'Found Folder: ') + lookup
        console.log('resolve',msg)
    }).catch((msg)=>{
        resultMsg.innerText = 'Unable to find File or Folder'
        console.log('reject',msg)
    })
    
}

///// Checking valid URL using regular expression
function checkURL(url) {
    return URLregex.test(url)
}
///// updated message according to validity
function updateField(check){
    if(!check){
        urlField.classList.add('is-invalid')
        urlField.classList.remove('is-valid')
    } else {
        urlField.classList.remove('is-invalid')
        urlField.classList.add('is-valid')
    }
}


//////////////////////// 
///// OLD async fetch call
async function findFileOrFolderOld(lookup){
    
    if(lookup=='' || lookup=='/'){
        resultMsg.innerText ='Empty path'
        return
    }
    let status = false;
    // get file or folder => getFoF
    var getFoF = await fetch('http://127.0.0.1:8887/src/'+lookup)
    .then(res=>{
        status=res.status
        if(status==200){
            resultMsg.innerText = fileRegex.test(lookup)?'Found File':'Found Folder'
        } else {
            resultMsg.innerText = 'Unable to find File or Folder'
        }
    })
    .catch(e=>console.log(e))
}